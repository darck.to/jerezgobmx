<!-- seccion de headers-->
<?php include_once('headers.php'); ?>

<body>

	<!-- Body -->

	<!--Seccion de Logo y Menu-->
	<div class="row no-m">

		<?php include_once('menu.php'); ?>

	</div>

	<!--Principal-->
	<div id="principalContent" class="row no-m">

		<div class="col s12 grey lighten-5">
			
			<div class="container">
					
				<div class="card transparent z-depth-0 ">
					<div class="card-content center-align">
						<span class="card-title blue-text text-darken-4">Boletines</span>
					</div>

					<div class="col s12 m8">
						
						<div class="card-content center-align white-text no-m no-p">

							<div id="boletinesArchivo" class="row container-pajinator"></div>

						</div>

					</div>

					<div class="hide-on-small-only col m4">
						
						<div class="card-content center-align white-text no-m no-p">

							<div class="row">

								<img class="responsive-img" src="https://via.placeholder.com/250x600"/>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="row no-m back-jerez">

		<div class="col s12 m6">
				
			<div class="card transparent z-depth-0">
				<div class="card-content center-align">
					<span class="card-title blue-text text-darken-4">Cont&aacute;ctanos</span>
				</div>
				<div class="card-content">
					<div class="row">
						<div class="col s12">
							<form>
								<div class="row no-m white z-depth-1 ten-p border-r-10">
									<div class="col s12 m6 black-text">
										<div class="input-field col s12 m6">
											<input id="nombre" type="text" class="validate" required>
			          						<label for="nombre">Nombre(s)</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="paterno" type="text" class="validate">
			          						<label for="paterno">Apellido Paterno</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="materno" type="text" class="validate">
			          						<label for="materno">Apellido Materno</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="telefono" type="text" class="validate">
			          						<label for="telefono">Tel&eacute;fono</label>
										</div>
									</div>
									<div class="col s12 m6 black-text">
										<div class="input-field col s12">
											<input id="correo" type="text" class="validate">
			          						<label for="correo">Correo @</label>
										</div>
										<div class="input-field col s12">
											<textarea id="mensaje" class="materialize-textarea"></textarea>
											<label for="mensaje">Mensaje</label>
										</div>
									</div>
									<div class="col s12 center-align">						
										<button class="btn z-depth-0 white black-text border-black-one" type="submit" name="action">Enviar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col s12 m6 white no-m no-p">
			
			<div class="card transparent z-depth-0 no-m">
				<div class="card-content no-p">
					<div class="row no-m">
						<div class="col s12 no-m no-p">
							<iframe class="portada-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3682.1143202447342!2d-102.99270858455577!3d22.649525885143248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8682effd71b6cc79%3A0x86c4c01269898e80!2sPresidencia+Municipal+de+Jerez!5e0!3m2!1sen!2smx!4v1542746482014" width="600" height="450" frameborder="0" style="border:0"></iframe>
						</div>
					</div>

				</div>
			</div>

		</div>

	</div>

	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>

<!-- seccion de inits-->
<?php include_once('init.php'); ?>

<script type="text/javascript" src="scripts/archivoLoad.js"></script>