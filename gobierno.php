<!-- seccion de headers-->
<?php include_once('headers.php'); ?>

<body>

	<!-- Body -->

	<!--Seccion de Logo y Menu-->
	<div class="row no-m">

		<?php include_once('menu.php'); ?>

	</div>

	<!-- seccion de inits-->
	<?php include_once('init.php'); ?>

	<?php

		$sub = $_GET['sub'];

		echo"
			<div class='row no-m'>
				<div class='container'>
					<div class='s12'>
						<div class='card transparent z-depth-0 '>
							<div class='card-content center-align'>
								<span class='card-title blue-text text-darken-4'>Gobierno</span>
								<div class='valign-wrapper blue-grey-text center-align section'><a href='.' class='blue-grey-text'><i class='material-icons'>home</i></a><i>&nbsp;".ucwords($sub)."</i></div>
								<div class='divider'></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		";

		include_once('templates/gobierno/'.$sub.'.php');

	?>
	
	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>