<!-- jQuery -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>

<!--Pajinator-->
<script type="text/javascript" src="js/pajinator.js"></script>

<!--Clipboard-->
<script type="text/javascript" src="js/clipboard.min.js"></script>

<!-- Load -->
<script type="text/javaScript" src="scripts/onLoad.js"></script>
<script type="text/javaScript" src="scripts/menu.js"></script>

<!-- Facebook SDK -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>