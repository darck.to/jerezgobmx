<?php
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../news/*_boletin_*.json');

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FECHA
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	//INIT 0
	$n = 0;

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				if ($n == 8) { die();}

				$date = $content['date'];
				$dia = substr($date, 0,2);
				$mes = substr($date, 2,2);
				$ano = substr($date, 4,4);

				$fecha = $dia."-".$mes."-".$ano;

				$title = $content['title'];

				$note = $content['note'];

				$foto1 = $content['foto1'];

				$folio = $content['folio'];

				$random = $content['random'];

				$file = $folio."_boletin_".$random."_".$date.".json";

				echo"
					<div class=\"col s12 m3\">
						<div class=\"card white z-depth-0 h-180\">
							<div class=\"card-image news-image-container\">
								<img class=\"activator handed nota-imagen\" src=\"".substr($foto1,9)."\">
								<span class=\"card-title nota-cabecera bgc-blue-1\">".substr($title,0,strrpos(substr($title,0,45),' '))."</span>
							</div>
							<div class=\"card-reveal\">
								<span class=\"card-title grey-text text-darken-4\">".$title."<i class=\"material-icons right\">close</i></span>
								<p class=\"left-align blue-grey-text text-darken-3\">".substr($note,0,strrpos(substr($note,0,120),' '))."<a class=\" handed navNota\" val=\"".$file."\">... mas</a></p>
							</div>
						</div>
					</div>
				";

				$n++;

			}

		} else {

		}

	}

?>

<script type="text/javascript" src="scripts/boletinesLoad.js"></script>