<?php

	//NOMBRE DE ARCHIVO
	$fileList = glob("../../assets/invitaciones/*.pdf");

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	//RECORREMOS LOS ARCHIVOS 3 VECES
	$n =  0;
	foreach($fileList as $filename){

		if ($n < 4) {
		   	//SI SOY ARCHIVOS PDF LOS LEEMOS PARA MOSTRARLOS
			if (file_exists($filename)) {

				echo"
					<div class=\"col s6 m3\">
						<div class=\"card bgc-blue z-depth-0\">
							<div class=\"card-content\">
								<div class=\"row\">
									<div class=\"col s12 white-text center-align\">
										<h6 class=\"white-text full-w\">".utf8_encode(str_replace(".pdf","" , str_replace("../../assets/invitaciones/", "", $filename)))."</h6>
										<a href=\"".$filename."\" target=\"_blank\"><img class=\"responsive-img\" src=\"img/doc.png\"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				";
				
			}
		}
		$n++;

	}

?>