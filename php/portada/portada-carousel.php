<?php
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../news/*_boletin_*.json');

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FECHA
	usort($fileList, function($a, $b) {return strcmp($b, $a);});

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				if ($content['portada'] == 1) {

					$date = $content['date'];
					$dia = substr($date, 0,2);
					$mes = substr($date, 2,2);
					$ano = substr($date, 4,4);

					$fecha = $dia."-".$mes."-".$ano;

					$title = $content['title'];

					$note = $content['note'];

					$foto1 = $content['foto1'];

					$folio = $content['folio'];

					$random = $content['random'];

					$file = $folio."_boletin_".$random."_".$date.".json";

					echo"
						<a class=\"carousel-item\">
							<div class=\"col s12 no-m no-p full-h p-r\">
								<div class=\"card white z-depth-0 no-m news-container handed navNota\" val=\"".$file."\">
									<div class=\"news-image-container\">
										<img class=\"portada-nota-imagen\" src=\"".substr($foto1,9)."\">
										<span class=\"card-title nota-cabecera nota-news bgc-blue-1\">".$title."</span>
									</div>
									<div class=\"news-content handed navNota\" val=\"".$file."\">".substr($note,0,strrpos(substr($note,0,250),' '))."...</div>
								</div>
							</div>
						</a>
					";

				} else {

					continue;
					
				}

			}

		} else {

		}

	}

?>

<script type="text/javascript" src="scripts/boletinesLoad.js"></script>