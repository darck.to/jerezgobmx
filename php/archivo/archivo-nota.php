<?php

	$nota = $_POST['nota'];

	//NOMBRE DE ARCHIVO
	$filename = '../../news/'.$nota;

   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($filename)) {
		
		$filename = file_get_contents($filename);
		$json = json_decode($filename, true);

		foreach ($json as $content) {

			$date = $content['date'];
			$dia = substr($date, 0,2);
			$mes = substr($date, 2,2);
			$ano = substr($date, 4,4);

			$fecha = $dia."-".$mes."-".$ano;

			$title = $content['title'];

			$note = $content['note'];

			$foto1 = $content['foto1'];
			$foto2 = $content['foto2'];

		}

	}

	echo"
		<div class='row no-m'>
			<div class='container'>
				<div class='s12'>
					<div class='card transparent z-depth-0 '>
						<div class='card-content center-align'>
							<span class='card-title blue-text text-darken-4'>Archivo</span>
							<div class='valign-wrapper blue-grey-text center-align section'><a href='.' class='blue-grey-text'><i class='material-icons'>home</i></a><i>&nbsp;Boletines | <i class=\"nota-title\">".ucwords($title)."</i></i></div>
							<div class='divider'></div>
						</div>
						<div class='card-content'>
							<img class='responsive-img' src='".substr($foto1,9)."'>
						</div>
						<div class='card-content'>
							<div class='row'>
								".$note."
							</div>
							<div class='row'>
								<div class='col s12 m6'>
									<img class='materialboxed responsive-img' src='".substr($foto2,9)."'>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	";

?>

<script type="text/javascript" src="scripts/notasLoad.js"></script>