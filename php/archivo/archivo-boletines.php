<?php
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../news/*_boletin_*.json');

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FECHA
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	//INIT 0
	$n = 0;

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$date = $content['date'];
				$dia = substr($date, 0,2);
				$mes = substr($date, 2,2);

				if ($mes == "1") { $mes == "ene";} elseif ($mes == "2") { $mes == "feb";} elseif ($mes == "3") { $mes == "mar";} elseif ($mes == "4") { $mes == "abr";} elseif ($mes == "5") { $mes == "may";} elseif ($mes == "6") { $mes == "jun";} elseif ($mes == "7") { $mes == "jul";} elseif ($mes == "8") { $mes == "ago";} elseif ($mes == "9") { $mes == "sep";} elseif ($mes == "10") { $mes == "oct";} elseif ($mes == "11") { $mes == "nov";} elseif ($mes == "12") { $mes == "dic";}

				$ano = substr($date, 4,4);

				$fecha = $dia."-".$mes."-".$ano;

				$title = $content['title'];

				$note = $content['note'];

				$foto1 = $content['foto1'];
				
				$folio = $content['folio'];

				$random = $content['random'];

				$file = $folio."_boletin_".$random."_".$date.".json";

				echo"
					<div class=\"col s12 m4 single-item\">
						<div class=\"card white z-depth-0 h-180\">
							<div class=\"card-image news-image-container handed navNota\" val=\"".$file."\">
								<img class=\"handed nota-imagen\" src=\"".substr($foto1,9)."\">
								<span class=\"card-title nota-cabecera bgc-blue-1\">".substr($title,0,strrpos(substr($title,0,45),' '))."</span>
								<span class=\"nota-fecha\">".$fecha."</span>
							</div>
						</div>
					</div>
				";

			}

		} else {

		}

	}

?>

<script type="text/javascript" src="scripts/boletinesLoad.js"></script>