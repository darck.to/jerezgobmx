<?php

	//NOMBRE DE ARCHIVO
	$fileList = glob("../../assets/decretos/*.pdf");

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			echo"
				<tr>
					<td class=\"left-align\">".str_replace(".pdf","" , str_replace("../../assets/decretos/", "", $filename))."</td>
					<td class=\"center-align\"><a href=\"".$filename."\" target=\"_blank\"><i class=\"material-icons small bgc-blue-2-text tiny no-m no-p\">picture_as_pdf</i></a></td>
				</tr>
			";

		} else {

		}

	}

?>