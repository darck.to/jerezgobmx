<?php

	include_once('../functions.php');

	$nivel = $_POST['nivel'];

	//PRIMER NIVEL
	if ($nivel == 1) {

		$dir = "../../assets/pnt/";

		$exclude = array( "desktop.ini",".","..","error_log","_notes" );

		if (is_dir($dir)) {
			
			$files = scandir($dir);
			
			foreach($files as $file){
				
				if(!in_array($file,$exclude)){

					//LEÉMOS LA INFORMACIÓN META DEL DIRECTORIO
					$filename = '../../assets/pnt/'.$file.'/meta_file.json';

				   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
					if (file_exists($filename)) {
						
						$filename = file_get_contents($filename);
						$json = json_decode($filename, true);

						foreach ($json as $content) {

							$descripcion = $content['descripcion'];

						}

					}

					echo"
						<div class=\"col s12 m6\">
							<div class=\"card fraccionesFeed handed\" val=\"".$file."\">
								<div class=\"card-content\">
									<div class=\"row\">
										<i class=\"material-icons\">local_library</i>
										".$file."
									</div>
								</div>
								<div class=\"card-action\">
									<div class=\"row\">
										<i class=\"grey-text text-darken-1 right\"><span>".$descripcion."</span></i>
									</div>
								</div>
							</div>
						</div>
					";
				}

			}

		}

	//SEGUNDO NIVEL
	} elseif ($nivel == 2) {

		$art = $_POST['art'];

		$dir = "../../assets/pnt/".$art."/";

		$exclude = array( "desktop.ini",".","..","error_log","_notes","meta_file.json" );

		if (is_dir($dir)) {
			
			$files = scandir($dir);

			sort($files, SORT_NUMERIC);

			echo"
			<div class=\"row\">
				<div class=\"col s12\">
					<i class=\"left\"><b class=\"grey-text text-lighten\">Selecciona una Fracci&oacute;n</b></i>
				</div>
			</div>
			<div class=\"row no\">
				<div class=\"col s12 grey lighten-5 center-align handed border-r-10 hideFeed\">
					<i class=\"material-icons grey-text\">arrow_drop_up</i>
				</div>
			</div>
			";
			
			foreach($files as $file){

				if(!in_array($file,$exclude)){

					//LEÉMOS LA INFORMACIÓN META DEL DIRECTORIO
					$filename = '../../assets/pnt/'.$art.'/'.$file.'/meta_file.json';

				   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
					if (file_exists($filename)) {

						$filename = file_get_contents($filename);
						$json = json_decode($filename, true);

						foreach ($json as $content) {

							$descripcion = $content['descripcion'];

						}

					}

					echo"
						<div class=\"col s4 m2 archivosFeed\" val=\"".$art."\" fra=\"".$file."\">
							<div class=\"card handed\">
								<div class=\"card-content no-p-b\">
									<div class=\"row center-align no-m\">
										<span><small class=\"grey-text text-lighten-1\">Fraccion&nbsp;&nbsp;</small>".nTOr($file)."</span>
									</div>	
								</div>
								<div class=\"card-content no-p ten-p-b\">
									<div class=\"row center-align\">
										<i class=\"material-icons small grey-text text-lighten-2\">arrow_drop_down</i>
									</div>
								</div>
							</div>
						</div>
					";

				}

			}

			echo"
				<div class=\"row\">
					<div class=\"col s12 grey lighten-5 center-align handed border-r-10 showFeed\">
						<i class=\"material-icons grey-text\">arrow_drop_down</i>
					</div>
				</div>
			";

		}

	//SEGUNDO NIVEL
	} elseif ($nivel == 3) {

		$art = $_POST['art'];

		$fra = $_POST['fra'];

		$dir = "../../assets/pnt/".$art."/".$fra."/";

		$exclude = array( "desktop.ini",".","..","error_log","_notes","meta_file.json" );

		if (is_dir($dir)) {

			//LEÉMOS LA INFORMACIÓN META DEL DIRECTORIO
			$filename = $dir.'meta_file.json';

		   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
			if (file_exists($filename)) {
				
				$filename = file_get_contents($filename);
				$json = json_decode($filename, true);

				$descripcion = $json[0]['descripcion'];

				echo "<h5>Justificaci&oacute;n&nbsp;|&nbsp;<small class=\"grey-text lighten-3\">".$descripcion."</small></h5><div class=\"divider\"></div>";

			}
			
			$dirOpen = scandir($dir);
			
			foreach($dirOpen as $dirName) {

				if(!in_array($dirName,$exclude)) {

					echo"
						<h6 class=\"ten-p\"><i class=\"bgc-blue-2-text\"><i class='material-icons left'>account_balance</i>".$art." | <b class=\"grey-text\">Fracci&oacuten ".$fra."</b></p></h6>
						<p class=\"archivos-ano ten-p\">Año ".$dirName."</p>
					";

					if (is_dir("../../assets/pnt/".$art."/".$fra."/".$dirName)) {
			
						$files = scandir("../../assets/pnt/".$art."/".$fra."/".$dirName);
						
						foreach($files as $file){

							if(!in_array($file,$exclude)) {
								
								echo"
									<p class=\"archivos-car ten-p\">Semestre ".$file."</p>
								";

								if (is_dir("../../assets/pnt/".$art."/".$fra."/".$dirName."/".$file."/")) {
			
									$dirtxt = "assets/pnt/".$art."/".$fra."/".$dirName."/".$file."/";
									$actives = scandir("../../assets/pnt/".$art."/".$fra."/".$dirName."/".$file."/");
									
									foreach($actives as $active){

										if(!in_array($active,$exclude)) {
											
											echo"
												<p class=\"archivos-fil ten-p valign-wrapper\">
													<i class=\"material-icons right blue-grey-text text-lighten-2 left\">insert_drive_file</i>
													".str_replace("_", " ",substr($active,0,-5))."&nbsp;<b class=\"grey-text text-lighten-2\">".substr($active,-4)."</b>
													<a class=\"right\" href=\"".$dirtxt.$active."\" target=\"_blank\"><i class=\"material-icons right grey-text text-lighten-2\">cloud_download</i></a>
													<i class=\"material-icons right grey-text text-lighten-2 handed copyLink\" data-clipboard-text=\"jerez.gob.mx/".$dirtxt.$active."\">link</i>
												</p>
											";

										}

									}
								
								}

							}

						}
					
					}

				}

			}

		}

	}

?>