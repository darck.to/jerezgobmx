<!-- seccion de headers-->
<?php include_once('headers.php'); ?>

<body>

	<!-- Body -->

	<!--Seccion de Logo y Menu-->
	<div class="row no-m">

		<?php include_once('menu.php'); ?>

	</div>

	<!--Principal-->
	<div id="principalContent"></div>
	
	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>

<!-- seccion de inits-->
<?php include_once('init.php'); ?>