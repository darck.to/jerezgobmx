$(document).ready(function(){

	$('.modal').modal();
	$('.sidenav').sidenav();
	$('.materialboxed').materialbox();
	$('.carouselFeria').carousel({
		dist: 0,
		numVisible: 3
	});

	autoplayFeria();

	procesaPrincipal();

});

function procesaPrincipal(e) {

	var url = window.location.pathname;

	console.log(url);

	if (url == '/jerezgobmx/') {

		$.ajax({
			url: 'templates/portada.php',
			dataType: "html",
			context: document.body,
			
			success: function(data) {

			  	$('#principalContent').html(data);
			  	
			},

			error: function(xhr, tst, err) {
			  console.log(err);
			}

		});

	}

}

//AUTOPLAY
function autoplayFeria() {
    $('.carouselFeria').carousel('next');
    setTimeout(autoplayFeria, 2000);
}