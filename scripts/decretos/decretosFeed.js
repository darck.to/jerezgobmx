$(document).ready(function(){
	
	decretosFeed();

	$('.tooltipped').tooltip();
	
});


function decretosFeed(e) {
	
	$.ajax({
		type: "POST",
		url: 'php/decretos/decretos-feed.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#decretosFeed').html(data);

		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});

}