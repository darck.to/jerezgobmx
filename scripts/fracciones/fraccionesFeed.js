$(document).ready(function(){
	
	articulosFeed();

	$('.tooltipped').tooltip();
	
});


function articulosFeed(e) {
	
	$.ajax({
		type: "POST",
		url: 'php/fracciones/fracciones-feed.php',
		data: {
	    	nivel: 1
	    },
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#articulosFeed').html(data);

			//CLICK PARA REVELAR FRACCIONES
			$('.fraccionesFeed').on( 'click' , function () {
				
				var art = $(this).attr('val');

				$.ajax({
					type: "POST",
					url: 'php/fracciones/fracciones-feed.php',
					data: {
				    	nivel: 2,
				    	art: art
				    },
					dataType: "html",
					context: document.body,
					
					success: function(result) {

						$('#archivosFeed').html('');

					  	$('#fraccionesFeed').html(result);

					  	$('.tooltipped').tooltip();

					  	$('.hideFeed').on( 'click' , function () {

							$('.archivosFeed').fadeOut();

						});

						$('.showFeed').on( 'click' , function () {

							$('.archivosFeed').fadeIn();

						});

					  	$('.archivosFeed').on( 'click' , function () {
				
							$('.newBox').remove();
							
							var art = $(this).attr('val');
							var fra = $(this).attr('fra');
	
							$.ajax({
								type: "POST",
								url: 'php/fracciones/fracciones-feed.php',
								data: {
							    	nivel: 3,
							    	art: art,
							    	fra: fra
							    },
								dataType: "html",
								context: document.body,
								
								success: function(files) {

									$('.archivosFeed').fadeOut();

									$('#archivosFeed').html(files);

									//COPY PASTA
									new ClipboardJS('.copyLink');

									$('.hideFeed').on( 'click' , function () {

										$('.archivosFeed').fadeOut();

									});

								},

								error: function(xhr, tst, err) {
								  console.log(err);
								}

							});

						});

					},

					error: function(xhr, tst, err) {
					  console.log(err);
					}

				});

			});

		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});

}