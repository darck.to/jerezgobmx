$(document).ready(function(){
	
	cargaBoletines();
	
});


function cargaBoletines(e) {
	//CARGA INFORMACION DE BOLETINES
	$.ajax({
		url: 'php/archivo/archivo-boletines.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#boletinesArchivo').html(data);

		  	//PAJINATOR
		  	$(".container-pajinator").pagify(9, ".single-item");
		  	
		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});
}