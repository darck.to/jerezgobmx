(function($) {
	//NAVIGATOR MENU
	$('.navNota').on( 'click' , function () {
		
		var nota = $(this).attr('val');

		console.log(nota);

		$.ajax({
			type: "POST",
			url: "php/archivo/archivo-nota.php",
			data: {
		    	nota: nota
		    },
			dataType: "html",
			context: document.body,

			success: function(data) {

				$('#principalContent').html(data);
				$('html, body').animate({ scrollTop: 0 }, 'fast');

			},

			error: function(xhr, tst, err) {
				console.log(err);
			}

		});
	  
	});

})(jQuery);