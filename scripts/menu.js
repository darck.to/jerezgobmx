(function($) {

	$('.sidenav').sidenav();

	//NAVIGATOR MENU
	$('.navigator').on( 'click' , function () {
		
		var ruta = $(this).attr('href');

		ruta = ruta.substr(2);

		sub = ruta.split('/');
		ruta = sub[0];
		subruta = sub[1];

		ruta = ('templates/' + ruta + '.php');

		$.ajax({
			type: "POST",
			url: ruta,
			data: {
		    	sub: subruta
		    },
			dataType: "html",
			context: document.body,

			success: function(data) {

				$('#principalContent').html(data);
				$('html, body').animate({ scrollTop: 0 }, 'fast');

			},

			error: function(xhr, tst, err) {
				console.log(err);
			}

		});
	  
	});

})(jQuery);