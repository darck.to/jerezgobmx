$(document).ready(function(){

	cargaCarousel();
	cargaDecretos();
	cargaBoletines();
	cargaInvitaciones();
	
});

function cargaCarousel(e) {
	//CARGA INFORMACION DE CAROUSEL
	$.ajax({
		url: 'php/portada/portada-carousel.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#carouselPrincipal').html(data);

		  	$('.carousel.carousel-slider').carousel({
				fullWidth: true
			});

			autoplay();
		  	
		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});
};

function cargaBoletines(e) {
	//CARGA INFORMACION DE BOLETINES
	$.ajax({
		url: 'php/portada/portada-boletines.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#boletinesPortada').html(data);
		  	
		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});
}

//AUTOPLAY
function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 4500);
}

function cargaDecretos(e){
	//CARGA INFORMACION DE BOLETINES
	$.ajax({
		url: 'php/portada/portada-decretos.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#decretosPortada').html(data);
		  	
		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});
}

function cargaInvitaciones(e){
	//CARGA INFORMACION DE INVITACIONES
	$.ajax({
		url: 'php/portada/portada-invitaciones.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#invitacionesPortada').html(data);
		  	
		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});
}