$(document).ready(function(){
	
	armonizacionFeed();

	$('.tooltipped').tooltip();
	
});


function armonizacionFeed(e) {
	
	$.ajax({
		type: "POST",
		url: 'php/armonizacion/armonizacion-feed.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#armonizacionFeed').html(data);

		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});

}