$(document).ready(function(){
	
	normativaFeed();

	$('.tooltipped').tooltip();
	
});


function normativaFeed(e) {
	
	$.ajax({
		type: "POST",
		url: 'php/gobierno/normativa-feed.php',
		dataType: "html",
		context: document.body,
		
		success: function(data) {

		  	$('#normativaFeed').html(data);

		},

		error: function(xhr, tst, err) {
		  console.log(err);
		}

	});

}