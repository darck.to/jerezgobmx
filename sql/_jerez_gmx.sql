-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-11-2018 a las 23:09:59
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `_jerez_gmx`
--
CREATE DATABASE IF NOT EXISTS `_jerez_gmx` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `_jerez_gmx`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `init_jgb`
--

CREATE TABLE IF NOT EXISTS `init_jgb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `init_nom` text COLLATE utf8_spanish_ci,
  `init_mai` text COLLATE utf8_spanish_ci,
  `init_pas` text COLLATE utf8_spanish_ci,
  `init_ran` int(11) NOT NULL,
  `init_linu` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'link de usuario',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `init_jgb`
--

INSERT INTO `init_jgb` (`id`, `init_nom`, `init_mai`, `init_pas`, `init_ran`, `init_linu`) VALUES
(4, 'Beto', 'darck.to@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, '3ET3X6JSBC57N3P7DGB1M5QS');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
