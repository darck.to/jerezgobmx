<div class="navbar-fixed valign-wrapper menu-130px">

	<nav class="bgc-gray z-depth-0 ten-p-top menu-130px">

		<!--SOCIAL MEDIA BUTTONS-->
		<div class="menu-social hide-on-small-only">
			
			<div class="row no-m">
				<div class="col s1 center-align">
					<a href="https://web.facebook.com/gobjerez/" target="_blank"><img class="handed ten-p-top" src="img/f_logo.png"></a>
				</div>
			</div>	

		</div>
			
		<div class="nav-wrapper z-depth-0">

			<div class="container">
					
				<a href="." class="brand-logo">
					<img class="hide-on-med-and-down show-on-large" src="img/logo-header-w.png">
					<img class="show-on-med-and-down hide-on-large-only" src="img/logo-header-m.png">
				</a>

				<a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

				<ul id="nav-mobile" class="right hide-on-med-and-down ten-p-top ul-drop">
					
					<!--<li><a href="#/tramites" class="handed grey-text text-darken-3 navigator"><b>TR&Aacute;MITES</b></a></li>-->
					<!--<li class="li-drop"><a href="feria.php" class="handed grey-text text-darken-3"><b>FERIA DE PRIMAVERA 2019</b></a></li>-->
					<li class="li-drop"><a class="handed grey-text text-darken-3"><b>GOBIERNO</b></a>
						<ul class="drop-ul">
					        <li class="blue-grey white-text"><a href="gobierno.php?sub=ayuntamiento" class="navigator" >H. AYUNTAMIENTO</a></li>
					        <!--<li class="blue-grey white-text"><a href="#/gobierno/direcciones" class="navigator" >DIRECCIONES</a></li>-->
					        <li class="blue-grey white-text"><a href="gobierno.php?sub=directorio" class="navigator" >DIRECTORIO</a></li>
					        <li class="blue-grey white-text"><a href="gobierno.php?sub=normativa" class="navigator" >NORMATIVA</a></li>
					        <li class="blue-grey white-text"><a href="gobierno.php?sub=decretos" class="navigator" >DECRETOS</a></li>
					    </ul>
					</li>
					<li class="li-drop"><a class="handed grey-text text-darken-3"><b>PRENSA</b></a>
						<ul class="drop-ul">
					        <li class="blue-grey white-text"><a href="prensa.php" class="navigator">ARCHIVO</a></li>
					    </ul>
					</li>
					<li class="li-drop"><a class="handed grey-text text-darken-3"><b>TRANSPARENCIA</b></a>
						<ul class="drop-ul">
					        <li class="blue-grey white-text"><a href="transparencia.php?sub=obligaciones" class="navigator" >TRANSPARENCIA</a></li>
					        <li class="blue-grey white-text"><a href="transparencia.php?sub=armonizacion" class="navigator" >ARMONIZACI&Oacute;N CONTABLE</a></li>
					        <li class="blue-grey white-text"><a href="transparencia.php?sub=licitaciones" class="navigator" >LICITACIONES</a></li>
					    </ul>
					</li>

					<li>
						<form>
							<div class="input-field white">
								<input id="search" type="search" placeholder="BUSQUEDA" required>
								<label class="label-icon" for="search"><i class="material-icons bgc-blue-2-text">search</i></label>
								<i class="material-icons">close</i>
							</div>
						</form>
					</li>

				</ul>

			</div>

		</div>

		<div class="menu-footer bgc-blue"></div>
		
	</nav>

</div>

<ul class="sidenav bgc-gray" id="mobile-nav">
	
	<!--<li class="li-drop"><a href="feria.php" class="handed grey-text text-darken-3"><b>FERIA DE PRIMAVERA 2019</b></a></li>-->
	<li class="li-drop"><a class="handed grey-text text-darken-3"><b>GOBIERNO</b></a>
		<ul class="drop-ul z-depth-1">
	        <li class="blue-grey white-text"><a href="#/gobierno/ayuntamiento" class="navigator sidenav-close" >H. AYUNTAMIENTO</a></li>
	        <!--<li class="blue-grey white-text"><a href="#/gobierno/direcciones" class="navigator sidenav-close" >DIRECCIONES</a></li>-->
	        <li class="blue-grey white-text"><a href="#/gobierno/directorio" class="navigator sidenav-close" >DIRECTORIO</a></li>
	        <li class="blue-grey white-text"><a href="#/gobierno/normativa" class="navigator sidenav-close" >NORMATIVA</a></li>
	        <li class="blue-grey white-text"><a href="#/gobierno/decretos" class="navigator sidenav-close" >DECRETOS</a></li>
	    </ul>
	</li>
	<!--<li><a href="#/tramites" class="handed grey-text text-darken-3 navigator sidenav-close"><b>TR&Aacute;MITES</b></a></li>-->
	<li class="li-drop"><a class="handed grey-text text-darken-3"><b>PRENSA</b></a>
		<ul class="drop-ul z-depth-1">
	        <li class="blue-grey white-text"><a href="#/archivo" class="navigator sidenav-close">ARCHIVO</a></li>
	    </ul>
	</li>
	<li class="li-drop"><a class="handed grey-text text-darken-3"><b>TRANSPARENCIA</b></a>
		<ul class="drop-ul z-depth-1">
	        <li class="blue-grey white-text"><a href="#/transparencia/obligaciones" class="navigator sidenav-close" >TRANSPARENCIA</a></li>
	        <li class="blue-grey white-text"><a href="#/transparencia/armonizacion" class="navigator sidenav-close" >ARMONIZACI&Oacute;N CONTABLE</a></li>
	        <li class="blue-grey white-text"><a href="#/transparencia/licitaciones" class="navigator sidenav-close" >LICITACIONES</a></li>
	    </ul>
	</li>

	<li>
		<form>
			<div class="input-field">
				<input id="search" type="search" required>
				<label class="label-icon" for="search"><i class="material-icons">search</i></label>
				<i class="material-icons">close</i>
			</div>
		</form>
	</li>

</ul>

<!--EMPTY MODAL-->
<div id="popUp" class="modal">

	<div class="modal-content">

		<h5 id="modalHeader" class="section"></h5>
		<div id="modalContent"></div>

	</div>
	
	<div class="modal-footer">

		<a class="modal-action modal-close handed btn red darken-1">Cerrar</a>
		
	</div>

</div>