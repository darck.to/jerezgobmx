<?php
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../../news/*boletin*.json');

	//ORDENAMOS EL ARREGLO DE ARCHIVOS POR FOLIO
	natsort($fileList);
	$fileList = array_reverse($fileList, false);

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$folio = $content['folio'];

				$fc = $content['fc'];
				
				$id = $content['random'];

				$date = $content['date'];
				$dia = substr($date, 0,2);
				$mes = substr($date, 2,2);
				$ano = substr($date, 4,4);

				$fecha = $dia."-".$mes."-".$ano;

				$title = $content['title'];

				$note = $content['note'];

				$foto1 = $content['foto1'];

				$foto2 = $content['foto2'];

				$star = $content['portada'];

				if ($star == 1) { $starColor = "amber-text"; } else { $starColor = "grey-text text-lighten-2"; }

				//NOMBRE DEL ARCHIVO
				$file = $folio."_boletin_".$id."_".$date.".json"; 

				echo"
					<li class='collection-item fiv-p-sides no-br'>
						<div class='row no-m'>
							<div class='col s12 m6 no-p left valign-wrapper'>
								<i class='material-icons left ".$starColor." handed editBoletin' act=1 val='".$file."'>star</i>
								".substr($title, 0,20)."
							</div>
							<div class='col s12 m6 no-p right'>
								<div class='buttons-container right valign-wrapper'>
								    <i class='material-icons right grey-text text-lighten-3 handed editBoletin' act=2 val='".$file."'>build</i>
								    <i class='right z-depth-1 '>
									    <img class='materialboxed' width='25' src='".substr($foto1,6)."'>
									</i>
									<i class='right z-depth-1 no-m'>
									    <img class='materialboxed' width='25' src='".substr($foto2,6)."'>
								    </i>
									<i class='material-icons right red-text text-lighten-3 handed editBoletin' act=3 val='".$file."'>cancel</i>
								</div>
							</div>
						</div>
					</li>
				";

			}

		} else {

		}

	}

?>

<script type="text/javascript" src="scripts/prensa/prensa_ultimas.js"></script>