<?php
	
	include('../../func/abre_conexion.php');

	//RECIBIMOS LA INFORMACION
	$action = mysqli_real_escape_string($mysqli, $_POST['action']);
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);

    //LEEMOS LA ACCIÓN
    //SI ES 1 LA MARCAMOS COMO FAVORITA PARA PORTADA (SOLO PUEDE HABER 3)
    if ($action == 1) {

    	$jsonString = file_get_contents('../../../news/'.$file);
		$data = json_decode($jsonString, true);

		$viejo = $data[0]['portada'];
		//LEÉMOS EL VALOR Y LO CAMBIOAMOS EN CONTRA
		if ($data[0]['portada'] == 1) {
			$portada = 0;
		} elseif ($data[0]['portada'] == 0) {
			$portada = 1;
		}

		$data[0]['portada'] = $portada;

		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data);
		file_put_contents('../../../news/'.$file, $newJsonString);

	//EDITAR BOLETIN
    } elseif ($action == 3) {
    	unlink('../../../news/'.$file);
    }

    include('../../func/cierra_conexion.php');

?>