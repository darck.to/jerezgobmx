<?php
	
	include('../../func/abre_conexion.php');	
	$_POST = json_decode(file_get_contents('php://input'), true);

	//RECIBIMOS LA INFORMACION
	$action = mysqli_real_escape_string($mysqli, $_POST['action']);
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);

    //LEEMOS LA ACCIÓN
    if ($action == 2) {

    	$jsonString = file_get_contents('../../../news/'.$file);
		$data = json_decode($jsonString, true);

		$resultados = array();

		$resultados[] = $data[0]['random'];
		$resultados[] = $data[0]['date'];
		$resultados[] = $data[0]['title'];
		$resultados[] = $data[0]['note'];
		$resultados[] = $data[0]['foto1'];
		$resultados[] = $data[0]['foto2'];

		print json_encode($resultados);

	//BORRAR BOLETÍN PERMANENTEMENTE
    }

   include('../../func/cierra_conexion.php');

?>