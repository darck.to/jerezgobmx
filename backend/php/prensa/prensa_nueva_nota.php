<?php
	include('../../func/abre_conexion.php');
	include('../../func/functions.php');

	if (empty($_POST['title'])) {die("Falta el T&iacute;tulo!"); }
	if (empty($_POST['note'])) {die("Falta la Nota"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$title = mysqli_real_escape_string($mysqli, $_POST['title']);
    $note = mysqli_real_escape_string($mysqli, $_POST['note']);
    date_default_timezone_set("America/Mexico_City");
	$fecha = Date('D-M-Y H:i');
	$date = Date('dmYHi');

	//ID ALEATORIO
	$random = generateRandomString();

	//GUARDAR IMAGENES EN CARPETA
    if(isset($_FILES['file1'])){
	    $errors= array();
	    $file_tmp =$_FILES['file1']['tmp_name'];
	    $file_type=$_FILES['file1']['type'];
	    $file_ext='PNG';
	    //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
	    $file_name = "img_1_".$random."_".$date.".".$file_ext;
	    $extensions= array("PNG");
	    
	    if(in_array($file_ext,$extensions)=== false){
	      $errors[]="extension not allowed, please choose a JPEG or PNG file.";
	    }

	    if(empty($errors)==true){
	      //LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
	      $file_name_final_1 = "../../../news/img/".$file_name;
	      $source_img = $file_tmp;
	      $destination_img = $file_name_final_1;
	      //LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
	      $file_compressed = compress($source_img, $destination_img, 80);
	      //LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
	      move_uploaded_file($file_compressed,".".$file_name);
	      //echo "Imagen Subida!";
	    }else{
	      print_r($errors);
	    }
	} else {
	  	//echo "Ocurrio un error, favor de consultar al administrador";
	  	$file_name_final_1 = "";
	}

	if(isset($_FILES['file2'])){
	    $errors= array();
	    $file_tmp =$_FILES['file2']['tmp_name'];
	    $file_type=$_FILES['file2']['type'];
	    $file_ext='PNG';
	    //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
	    $file_name = "img_2_".$random."_".$date.".".$file_ext;
	    $extensions= array("PNG");
	    
	    if(in_array($file_ext,$extensions) === false){
	      $errors[]="extension not allowed, please choose a JPEG or PNG file.";
	    }

	    if(empty($errors)==true){
	      //LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
	      $file_name_final_2 = "../../../news/img/".$file_name;
	      $source_img = $file_tmp;
	      $destination_img = $file_name_final_2;
	      //LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
	      $file_compressed = compress($source_img, $destination_img, 80);
	      //LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
	      move_uploaded_file($file_compressed,".".$file_name);
	      //echo "Imagen Subida!";
	    }else{
	      print_r($errors);
	    }
	} else {
	  	//echo "Ocurrio un error, favor de consultar al administrador";
	  	$file_name_final_2 = "";
	}

	//GUARDAMOS EL ARCHIVO DE LA NOTA
    $cabecera[] = array('folio'=> $folio,'fc'=> $fecha, 'random'=> $random, 'date'=> $date, 'title'=> $title, 'note'=> $note, 'foto1'=> $file_name_final_1, 'foto2'=> $file_name_final_2, 'portada'=> 0);

	//NOMBRE DE ARCHIVO
	$fileName = '../../../news/'.$folio.'_boletin_'.$random.'_'.$date.'.json';
	//INCREMENTAMOS EL FOLIO
	masFolio();
	if (file_exists($fileName)) {
	} else {
		$fileChat = fopen($fileName, 'w') or die ('No se guardo el archivo');
		fwrite($fileChat, json_encode($cabecera));
		fclose($fileChat);
		echo "<span>NOTA GUARDADA<i class=\"material-icons right\">beenhere</i></span>";
	}

	include('../../func/cierra_conexion.php');

?>