<?php
	
	include('../../func/abre_conexion.php');	

	//RECIBIMOS LA INFORMACION
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);

    if (file_exists($file)) {
		$jsonString = file_get_contents($file);
		$data = json_decode($jsonString, true);
		$resultados = $data[0]['descripcion'];
	} else {
		$fileOpen = fopen($file, 'w') or die ('No se guardo el archivo');
		fwrite($fileOpen, json_encode($cabecera));
		fclose($fileOpen);
		$resultados = "";
	}

	echo $resultados;

   include('../../func/cierra_conexion.php');

?>