<?php
	
	include('../../func/abre_conexion.php');

	//RECIBIMOS LA INFORMACION
    $file = mysqli_real_escape_string($mysqli, $_POST['file']);
	$desc = mysqli_real_escape_string($mysqli, $_POST['desc']);

	$jsonString = file_get_contents($file);
	$data = json_decode($jsonString, true);

	$data[0]['descripcion'] = $desc;

	//LO VOLVEMOS A GUARDAR
	$newJsonString = json_encode($data);
	if (file_put_contents($file, $newJsonString)) {
		echo $file.", fue cargado correctamente!";
	} else {
		echo "Error, revisa tu codigo";
	}

    include('../../func/cierra_conexion.php');

?>