<?php

	$dir = "../../../assets/pnt/";

	$exclude = array( ".","..","error_log","_notes","meta_file.json","desktop.ini" );

	if (is_dir($dir)) {
		
		$files = scandir($dir);
		
		foreach($files as $dir){
			
			if(!in_array($dir,$exclude)){

				//LEÉMOS LA INFORMACIÓN META DEL DIRECTORIO
				$filename = '../../../assets/pnt/'.$dir.'/meta_file.json';

				echo"
					<li class=\"collection-item grey lighten-3\">".$dir."<i class=\"material-icons right grey-text text-lighten-1 handed showEdit\" val=\"".$filename."\">edit</i></li>
				";

				$subdir = "../../../assets/pnt/".$dir."/";

				if (is_dir($subdir)) {
		
					$files = scandir($subdir);

					array_multisort($files,SORT_NUMERIC, SORT_ASC);
					
					foreach($files as $subdir){
						
						if(!in_array($subdir,$exclude)){

							//LEÉMOS LA INFORMACIÓN META DEL DIRECTORIO
							$filename = '../../../assets/pnt/'.$dir.'/'.$subdir.'/meta_file.json';

							echo"
								<li class=\"collection-item\">Fracci&oacute;n ".$subdir."<i class=\"material-icons right grey-text text-lighten-1 handed showEdit\" val=\"".$filename."\">edit</i></li>
							";
						}

					}

				}

			}

		}

	}

?>