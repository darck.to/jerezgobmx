<?
	session_start();
	$rango = $_SESSION['usuario_rango'];

?>

<div class="container">
<div class="row">
	<div class="col s12">
		<h5><i class="material-icons left grey-text">photo_library</i>Boletines |<i class="grey-text"> editor</i></h5>
	</div>
	<div class="col s12 m12 l8">
		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content blue-grey darken-2 white-text right-align">
						<div class="row no-m">
							<h5 class="left">Editor:</h5>
							<h6 class="right"><i class="material-icons right handed clearBoletin t-s-b">autorenew</i><i class="grey-text text-lighten-1">Limpiar/Nuevo</i></h6>
						</div>
					</div>
					<div class="card-content">
						<div class="editable-textarea">
							<div class="row no-m fifte-p-top">
								<div class=" input-field col s12 no-m">
									<input id="random" type="hidden">
									<input id="date" type="hidden">
									<input id="title" type="text" class="validate">
									<label for="title">T&iacute;tulo</label>
								</div>
							</div>
						</div>
					</div>
					<div class="card-content">
						<div class="editable-textarea">
							<div class="fifte-p-top editable"></div>
						</div>
					</div>
					<div class="card-content">
						<div class="editable-textarea">
							<div class="row">
								<div class="col s12">
									<h6 class="left grey-text">Fotograf&iacute;as:</h6>
								</div>
								<div class="col s12">
									<div class="row no-m">
										<i class='left p-r'>
											<i class="material-icons circle tiny red white-text handed del-img delImg1" val>close</i>
										    <img class='materialboxed img1' width='80' src=''>
										</i>
										<i class='left p-r'>
											<i class="material-icons circle tiny red white-text handed del-img delImg2" val>close</i>
										    <img class='materialboxed img2' width='80' src=''>
									    </i>
									</div>
								</div>
							</div>
							<div class="row no-m">
								<div class="editable-textarea">
									<div class="row no-m">
										<div class="col s12 m6 file-field input-field">
											<div class="btn btnImg1">
												<span>Portada</span>
												<input id="foto1" type="file">
											</div>
											<div class="file-path-wrapper">
												<input id="filepath1" class="file-path validate" type="text">
											</div>
										</div>
										<div class="col s12 m6 file-field input-field">
											<div class="btn btnImg2">
												<span>Interior</span>
												<input id="foto2" type="file">
											</div>
											<div class="file-path-wrapper">
												<input id="filepath2" class="file-path validate" type="text">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action action-butons">
						<a class="btn-floating halfway-fab waves-effect waves-light green guardaNota"><i class="material-icons">add</i></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col s12 m12 l4">
		<div class="card">
			<div class="card-content light-blue darken-3 right-align no-m">
				<div class="row no-m">
					<h6 class="white-text left">&Uacute;ltimas</h6>
					<i class="material-icons right datepicker handed">date_range</i>
				</div>
			</div>
			<div class="card-content no-p">
				<div class="row no-p no-m">
					<ul id="ultimosCollection" class="collection">
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>


<script type="text/javascript" src="scripts/prensa/prensa.js"></script>