<?
	session_start();
	$rango = $_SESSION['usuario_rango'];

?>

<div class="container">
	<div class="row">
		<div class="col s12">
			<h5><i class="material-icons left grey-text">folder</i>Articulos | Fracciones</h5>
		</div>
		
		<div class="col s12 m4">
			<div class="card">
				<div class="card-content light-blue darken-3 right-align no-m">
					<div class="row no-m">
						<h6 class="white-text left">Listado</h6>
						<h6 class="right"><i class="material-icons right handed refreshList t-s-b">autorenew</i><i class="white-text">Refresh</i></h6>
					</div>
				</div>
				<div class="card-content no-p">
					<div class="row no-p no-m">
						<ul id="fraCollection" class="collection">
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col s12 m8">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content blue-grey darken-2 white-text right-align">
							<div class="row no-m">
								<h5 class="left">Contenido del archivo</h5>
								<p id="titulo" class="left grey-text text-lighten-1"></p>
							</div>
						</div>
						<div class="card-content">
							<div class="editable-textarea">
								<div class="row no-m fifte-p-top">
									<div class=" input-field col s12 no-m">
										<input id="descripcion" type="text" class="validate">
										<label for="descripcion">Descripci&oacute;n</label>
									</div>
								</div>
							</div>
						</div>
						<div class="card-action action-butons">
							<a class="btn-floating halfway-fab waves-effect waves-light green hide saveArchivo" val=""><i class="material-icons">add</i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<script type="text/javascript" src="scripts/transparencia/transparencia.js"></script>