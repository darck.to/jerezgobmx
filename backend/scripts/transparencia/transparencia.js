$(document).ready(function(){
	var editor = new MediumEditor('.editable', {
		toolbar: {
	        buttons: ['bold', 'italic', 'underline', 'h2', 'h4', 'quote']
	    }
	});

	fraCollection();
});

function fraCollection(e) {

	$.ajax({
	    cache: 'false',
	    url: "php/transparencia/transparencia_ultimas.php",
	    context: document.body,
	    success: function(data) {

	      $('#fraCollection').html(data);

	      //DESPUES DE MOSTRAR LAS FRACCIONES LE PONEMOS LA ACCION AL BOTON DE EDIT
	      $('.showEdit').on( 'click' , function() {

	      	var file = $(this).attr('val');

			$.ajax({
				method: 'post',
			    url: 'php/transparencia/transparencia_muestra.php',
			    data: { 
			    	file: file
			    },
				context: document.body,
				success: function(data) {
					$('html, body').animate({ scrollTop: 0 }, 'fast');
					$('#titulo').html(file);
					$('#descripcion').focus();
					$('#descripcion').val(data);
					$('.saveArchivo').attr('val',file);
					$('.saveArchivo').removeClass('hide');
				},
				error: function(xhr, tst, err) {
					console.log(err);
				}

			});

		});

	      
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
	})

}

$('.saveArchivo').on( 'click' , function() {

	var file = $(this).attr('val');
	var desc = $('#descripcion').val();

	var form_data = new FormData();

    form_data.append("file", file);
    form_data.append("desc", desc);

	$.ajax({
		type: 'post',
		url: 'php/transparencia/transparencia_edita.php',
		data: form_data,
		cache: false,
        contentType: false,
        processData: false,
		dataType: "html",
		context: document.body,

		success: function(data) {
			M.toast({html: data});
			$('#titulo').html('');
			$('#descripcion').val('');
			$('.saveArchivo').attr('val','');
			$('.saveArchivo').addClass('hide');
		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

});

$('.clearBoletin').on( 'click' , function() {

});