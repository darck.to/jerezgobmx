$(document).ready(function(){
	$('.materialboxed').materialbox();
});

$('.editBoletin').on( 'click' , function() {

	var action = $(this).attr('act');
	var file = $(this).attr('val');

	if (action == 1) {

		//MARCAR COMO FAVORITO PARA PORTADA
		$.ajax({
			method: 'post',
		    url: 'php/prensa/prensa_edita_nota.php',
		    data: { 
		    	action: action,
		    	file: file
		    },
		    dataType: "html",
		    context: document.body,
		    success: function(data) {
		    	cargaUltimas();
		    },
		    error: function(xhr, tst, err) {
		      console.log(err);
		    }
		});
	}

	//EDITAR BOLETIN
	if (action == 2) {
    	$.ajax({
			method: 'post',
		    url: 'php/prensa/prensa_edita_boletin.php',
		    data: JSON.stringify({ 
		    	action: action,
		    	file: file
		    }),
		    contentType: "application/json",
			context: document.body,
		    success: function(data) {
		    	data = JSON.parse(data);

		    	$('#random').val(data[0]);
		    	$('#date').val(data[1]);
		    	$('#title').val(data[2]);
		    	$('#title').focus();
		    	$('.editable').html(data[3]);
		    	$('.img1').attr('src',data[4].substr(6));
		    	$('.img2').attr('src',data[5].substr(6));
		    	$('#foto1').prop('disabled',true);
		    	$('#foto2').prop('disabled',true);
		    	$('.btn').addClass('grey');
		    	$('.delImg1').css('display','block').attr('val',data[4].substr(18,33));
		    	$('.delImg2').css('display','block').attr('val',data[5].substr(18,33));
		    	//AGREGA EL BOTON DE EDICION Y BORRA EL DE GUARDADO NUEVO
		    	$('.action-butons').html('<a class="btn-floating halfway-fab waves-effect waves-light blue editaNota"><i class="material-icons">edit</i></a>');
		    	
		    },
		    error: function(xhr, tst, err) {
		      console.log(err);
		    }
		});
	}

	//ELIMINAR BOLETÍN
	if (action == 3) {
		var c = confirm("¿Deseas eliminar este bolet\xEDn? \nEsta acci\xF3n es irreversible");
	    if (c == true) {
	    	$.ajax({
				method: 'post',
			    url: 'php/prensa/prensa_edita_nota.php',
			    data: { 
			    	action: action,
			    	file: file
			    },
			    dataType: "html",
			    context: document.body,
			    success: function(data) {
			    	cargaUltimas();
			    },
			    error: function(xhr, tst, err) {
			      console.log(err);
			    }
			});
	    } else {
			return false;
	    }
	}

});

//CARGA ULTIMAS NOTAS
function cargaUltimas(e) {

	$.ajax({
	    cache: 'false',
	    url: "php/prensa/prensa_ultimas.php",
	    context: document.body,
	    success: function(data) {

	      $('#ultimosCollection').html(data);
	      
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
	})

}