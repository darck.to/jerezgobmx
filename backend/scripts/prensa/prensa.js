$(document).ready(function(){
	var editor = new MediumEditor('.editable', {
		toolbar: {
	        buttons: ['bold', 'italic', 'underline', 'h2', 'h4', 'quote']
	    }
	});

	$('.datepicker').datepicker();
	$('.materialboxed').materialbox();

	cargaUltimas();
});

$('.guardaNota').on( 'click' , function() {

	var title = $('#title').val();
	var note = $('.editable').html();

	var $foto1 = $("#foto1");
	var $foto2 = $("#foto2");

	if (parseInt($foto1.get(0).files.length) > 2){

		M.toast({html: "Se permite un máximo de 1 archivos por carga"});
		return false;

	}

	if (parseInt($foto2.get(0).files.length) > 2){

		M.toast({html: "Se permite un máximo de 1 archivos por carga"});
		return false;

	}

	var form_data = new FormData();

    form_data.append("file1", document.getElementById('foto1').files[0]);
    form_data.append("file2", document.getElementById('foto2').files[0]);

    form_data.append("title", title);
    form_data.append("note", note);   

	$.ajax({
		type: 'post',
		url: 'php/prensa/prensa_nueva_nota.php',
		data: form_data,
		cache: false,
        contentType: false,
        processData: false,
		dataType: "html",
		context: document.body,

		success: function(data) {
			M.toast({html: data});
			$('#title').val('');
			$('.editable').html('');
			$('#foto1').val('');
			$('#foto2').val('');
			$('.file-path').val('');

			cargaUltimas();
		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

});

//CARGA ULTIMAS NOTAS
function cargaUltimas(e) {

	$.ajax({
	    cache: 'false',
	    url: "php/prensa/prensa_ultimas.php",
	    context: document.body,
	    success: function(data) {

	      $('#ultimosCollection').html(data);
	      
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
	})

}

$('.clearBoletin').on( 'click' , function() {
	$('#title').val('');
	$('.editable').html('');
	$('#foto1').val('');
	$('#foto2').val('');
	$('.file-path').val('');
	$('.img1').attr('src','');
	$('.img2').attr('src','');
	$('#foto1').prop('disabled',false);
	$('#foto2').prop('disabled',false);
	$('.del-img').css('display','none').attr('val','');
	$('.btn').removeClass('grey');
	$('.btn').addClass('teal');
	$('.action-butons').html('<a class="btn-floating halfway-fab waves-effect waves-light green guardaNota"><i class="material-icons">add</i></a>');
});

$('.delImg1').on( 'click' , function() {
	var c = confirm("¿Deseas eliminar esta imagen? \nEsta acci\xF3n es irreversible");
    if (c == true) {
		$('#foto1').prop('disabled',false);
		$('.btnImg1').removeClass('grey');
		$('.btnImg1').addClass('teal');
    } else {
		return false;
    }
});

$('.delImg2').on( 'click' , function() {
	var c = confirm("¿Deseas eliminar esta imagen? \nEsta acci\xF3n es irreversible");
	if (c == true) {
		$('#foto2').prop('disabled',false);
		$('.btnImg2').removeClass('grey');
		$('.btnImg2').addClass('teal');
    } else {
		return false;
    }
});