//REGISTRAMOS EL USUARIO
$('form#capturaUser').submit(function(event) {
  event.preventDefault();

  var formNombre = $(this).attr('name');
  var formData = new FormData(document.getElementById("capturaUser"));
  var formMethod = $(this).attr('method');
  var rutaScrtip = $(this).attr('action');

  //COMPARAMOS LAS CADENAS DE PASSWORD
  var pass = $('#password').val();
  var pass2 = $('#password2').val();

  if (pass != pass2) {

    M.toast({html: 'Las contraseñas no coinciden'})

  } else {

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      dataType: "html"
    });

    // handle the responses
    request.done(function(data) {
      M.toast({html: data});
      window.location.href = (".").delay( 1000 );
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('form[name="' + formNombre + '"]').trigger('reset');
    });

  }

});