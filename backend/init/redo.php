<?php
	session_start();
  //FECHA Y HORA
  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('H:i Y-m-d');
  if (isset($_SESSION['usuario_nombre'])) {
  } else {
?>

<form id="formRedo" name="formRedoing" action="keyup/sendRedo.php" method="post" autocomplete="off">

	<div class="form-group">
  	<input class="form-control" type="text" name="usuario_email" placeholder="Escribe tu correo registrado"/><br />
	</div>
	<div class="form-group">
  	<button class="waves-effect waves-light btn blue darken-3" name="enviar"><i class="material-icons left">redo</i>Recuperar</button>
	</div>
  
</form>

<!--Login js para comprobar la sesion y recargar el index con un usuario-->
<script type="text/javascript" src="scripts/logindo.js"></script>

<?php	
	}
?>