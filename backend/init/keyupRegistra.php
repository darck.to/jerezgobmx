<?php
  // incluimos el archivo de conexión a la Base de Datos
  include('abre_conexion.php');

  include('../func/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
	  
  // creamos una función que nos parmita validar el email
  function valida_email($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}
		
    // Procedemos a comprobar que los campos del formulario no estén vacíos
	  $sin_espacios = count_chars($_POST['usuario_nombre'], 1);
    // comprobamos que el campo usuario_nombre no tenga espacios en blanco
	  if (empty($_POST['usuario_nombre'])) {
	    echo "No haz ingresado tu usuario. <a href='javascript:history.back();'>Reintentar</a>";
    // comprobamos que el campo usuario_clave no esté vacío
	  } elseif (empty($_POST['usuario_clave'])) {
	    echo "No haz ingresado contraseña. <a href='javascript:history.back();'>Reintentar</a>";
    // comprobamos que las contraseñas ingresadas coincidan
	  } elseif ($_POST['usuario_clave'] != $_POST['usuario_clave_conf']) {
	    echo "Las contraseñas ingresadas no coinciden. <a href='javascript:history.back();'>Reintentar</a>";
    // validamos que el email ingresado sea correcto
	  } elseif (!valida_email($_POST['usuario_email'])) {
      echo "El email ingresado no es válido. <a href='javascript:history.back();'>Reintentar</a>";
	  } else {
      // "limpiamos" los campos del formulario de posibles códigos maliciosos
	    $usuario_nombre = mysqli_real_escape_string($mysqli, $_POST['usuario_nombre']);
	    $usuario_clave = mysqli_real_escape_string($mysqli, $_POST['usuario_clave']);
	    $usuario_email = mysqli_real_escape_string($mysqli, $_POST['usuario_email']);
      
	    // comprobamos que el usuario ingresado no haya sido registrado antes
	    $sql = $mysqli->query("SELECT init_nom FROM init_jgb WHERE init_nom ='".$usuario_nombre."'");
	    if ($sql->num_rows > 0) {
	      echo "El nombre usuario elegido ya ha sido registrado anteriormente. <a href='javascript:history.back();'>Reintentar</a>";
	    } else {
	      $usuario_clave = md5($usuario_clave); // encriptamos la contraseña ingresada con md5
	      
		  // ingresamos los datos a la BD
	      $sqlReg = $mysqli->query("INSERT INTO init_jgb (init_nom, init_pas, init_mai, init_ran, init_linu) VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$usuario_email."', ".$usuario_rango.", ".$usuario_tipo.")");
	      if($sqlReg) {
	        echo "Datos ingresados correctamente";
	      } else {
	        echo "ha ocurrido un error y no se registraron los datos</br>";
          	echo("</br>Error description: " . mysqli_error($mysqli));
	      }
	    }

	  }

  // incluimos el archivo de conexión a la Base de Datos
  include('cierra_conexion.php');
?>