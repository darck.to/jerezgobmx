<?php
  session_start();

  include ('abre_conexion.php');

  if (empty($_POST['usuario_nombre']) || empty($_POST['usuario_clave'])) {
    echo "El usuario o la contraseña no han sido ingresados correctamente!";
  } else {
    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $usuario_nombre = mysqli_real_escape_string($mysqli,$_POST['usuario_nombre']);
    $usuario_clave = mysqli_real_escape_string($mysqli,$_POST['usuario_clave']);
    $usuario_clave = md5($usuario_clave);
    
    // comprobamos que los datos ingresados en el formulario coincidan con los de la BD
    $sql = $mysqli->query("SELECT * FROM init_jgb WHERE init_nom = '".$usuario_nombre."' AND init_pas = '".$usuario_clave."'");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $_SESSION['usuario_nombre'] = $row["init_nom"];
      $_SESSION['usuario_email'] = $row['init_mai'];
      $_SESSION['usuario_rango'] = $row["init_ran"];
      if (isset($row["number"])) {
        $_SESSION['number'] = $row["number"];
      } else {
        $_SESSION['number'] = "NULL";
      }
      
      if ($sql) {
        echo "<br>Bienvenido ".strtoupper($_SESSION['usuario_nombre']);
      } else {
        printf("<br>Errormessage: %s\n", $mysqli->error);
      }
    } else {
      echo"Nombre o contraseña incorrectos!";
    }
  }
  include ('cierra_conexion.php');
?>