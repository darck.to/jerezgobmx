
<!-- jQuery -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>

<!--Medium Editor-->
<script src="js/medium-editor.min.js"></script>

<!-- Load -->
<script type="text/javaScript" src="scripts/menu.js"></script>
<script type="text/javaScript" src="scripts/onLoad.js"></script>
<script type="text/javaScript" src="init/login.js"></script>