<div class="navbar-fixed">

	<nav>
			
		<div class="nav-wrapper blue-grey darken-3 z-depth-0">

			<a href="." class="brand-logo">
				<h5>Panel</h5>
			</a>

			<a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

			<ul id="nav-mobile" class="right hide-on-med-and-down">
				
				<li><a href="."><i class="material-icons">home</i></a></li>
				<li><a class="handed" onclick="initFunction()"><i class="material-icons">vpn_key</i></a></li>

				<?php
				
					session_start();
					if (isset($_SESSION['usuario_nombre'])) {

				?>

				<li class="handed sidenav-close" onclick="menuNav('template/prensa/prensa.php')"><a><div class="valign-wrapper"><i class="material-icons">photo_library</i>Boletines</div></a></li>
				<li class="handed sidenav-close" onclick="menuNav('template/transparencia/transparencia.php')"><a><div class="valign-wrapper"><i class="material-icons">find_in_page</i>Transparencia</div></a></li>

				<?php

				} else {

				}

				?>

			</ul>


		</div>

	</nav>

</div>

<ul class="sidenav grey lighten-4" id="mobile-nav">
	
	<li class="handed"><a href="."><i class="material-icons">home</i></a></li>

<?php

if (isset($_SESSION['usuario_nombre'])) {

?>

	<li class="handed sidenav-close" onclick="menuNav('template/prensa/prensa.php')"><a><div class="valign-wrapper"><i class="material-icons">photo_library</i>Boletines</div></a></li>
	<li class="handed sidenav-close"><a><div class="valign-wrapper"><i class="material-icons">business</i>Gobierno</div></a></li>
	<li class="handed sidenav-close" onclick="menuNav('template/transparencia/transparencia.php')"><a><div class="valign-wrapper"><i class="material-icons">find_in_page</i>Transparencia</div></a></li>

<?php

} else {

}

?>


	<li><a class="handed sidenav-close" onclick="initFunction()"><i class="material-icons">vpn_key</i></a></li>

</ul>

<script type="text/javascript" src="scripts/menu.js"></script>