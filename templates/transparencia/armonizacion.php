<div class="container">
	<div class="row">
		
		<div class="col m1 offset-m10">
			
			<div class="card z-depth-0">
				<a href="http://cacezac.gob.mx/" target="_blank">
					<div class="card-content center-align no-m no-p">
						<img class="circle tooltiped" data-position="top" data-tooltip="Consejo de Armanizaci&oacute; Contable del Estado de Zacatecas" width="50" src="img/cacezac-l.png">
					</div>
				</a>
			</div>

		</div>

		<div class="col s12 m1">
			
			<div class="card z-depth-0">
				<a href="http://www.asezac.gob.mx/" target="_blank">
					<div class="card-content center-align no-m no-p">
						<img class="circle tooltiped" data-position="top" data-tooltip="Auditoria Superior del Estado" width="50" src="img/ase-l.png">
					</div>
				</a>
			</div>

		</div>

	</div>

	<div class="row">
		
		<div class="col s12 m8">
			<p>Establecer los criterios generales que  regir&aacute;n  la  contabilidad  gubernamental  y  la  emisi&oacute;n  de  informaci&oacute;n  financiera  de  los  entes  p&uacute;blicos, con el fin de lograr su adecuada armonizaci&oacute;n.</p>
			<p class="right-align section"><small><b>Del objetivo y las definiciones de la <br>Ley General de Contabilidad Gubernamental</b></small></p>
		</div>

	</div>

</div>

<div class="row section">
	<div class="container">
		<div class="col s12">
			<div class='valign-wrapper center-align section'>
				<a class='blue-grey-text'>
					<i class='material-icons'>account_balance</i>
				</a>
				<i class='blue-grey-text'>&nbsp;Obligaciones</i>
			</div>
			<div class="divider"></div>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		
		<div class="col s12">

			<table class="striped armonizacion-table">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th class="center-align" colspan="4">2019</th>
						<th class="center-align" colspan="4">2018</th>
					</tr>
					<tr class="grey lighten-3">
						<th class="right-align">Trimestres:</th>
						<th class="center-align">1</th>
						<th class="center-align">2</th>
						<th class="center-align">3</th>
						<th class="center-align">4</th>
						<th class="center-align">1</th>
						<th class="center-align">2</th>
						<th class="center-align">3</th>
						<th class="center-align">4</th>
						
					</tr>
				</thead>

				<tbody id="armonizacionFeed">				
				</tbody>
			</table>

		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/armonizacion/armonizacionFeed.js"></script>