<div class="container">

	<div class="row">

		<div class="col s12 m3 offset-m3">			

			<div class="card z-depth-0">

				<a href="http://infomexzacatecas.org.mx/Infomex/" target="_blank">

					<div class="card-content center-align">

						<img class="circle" width="100" src="img/infomex-l.png">

					</div>

					<div class="card-action center-align">

						<i class="black-text handed">INFOMEX</i>

					</div>

				</a>

			</div>

		</div>

		<div class="col s12 m3">			

			<div class="card z-depth-0">

				<a href="https://www.plataformadetransparencia.org.mx/web/guest/inicio" target="_blank">

					<div class="card-content center-align">

						<img class="circle" width="100" src="img/pnt-l.png">

					</div>

					<div class="card-action center-align">

						<i class="black-text handed">PLATAFORMA NACIONAL DE TRANSPARENCIA</i>

					</div>

				</a>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col s12 m8 offset-m3">

			<blockquote><h6>La Ley General busca promover, fomentar y difundir la cultura de la transparencia en el ejercicio de la funci&oacute;n p&uacute;blica, el acceso a la informaci&oacute;n, la participaci&oacute;n ciudadana, asd&iacute; como la rendici&oacute;n de cuentas, a travd&eacute;s del establecimiento de pold&iacute;ticas p&uacute;blicas y mecanismos que garanticen la publicidad de informaci&oacute;n oportuna, verificable, comprensible, actualizada y completa.</h6>

			<p class="right-align"><small><b>Del derecho de acceso a la informaci&oacute;n<br>Ley de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica del Estado de Zacatecas</b></small></p>

			</blockquote>

		</div>

	</div>

</div>