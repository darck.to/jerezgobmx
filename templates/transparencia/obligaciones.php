<div class="container">

	<div class="row">
		
		<div class="col m1 offset-m10">
			
			<div class="card z-depth-0 no-m no-p">
				<a href="http://infomexzacatecas.org.mx/Infomex/" target="_blank">
					<div class="card-content center-align no-m- no-p">
						<img class="circle tooltipped" data-position="top" data-tooltip="Infomex" width="50" src="img/infomex-l.png">
					</div>
				</a>
			</div>

		</div>

		<div class="col m1">
			
			<div class="card z-depth-0 no-m no-p">
				<a href="https://www.plataformadetransparencia.org.mx/web/guest/inicio" target="_blank">
					<div class="card-content center-align no-m- no-p">
						<img class="circle tooltipped" data-position="top" data-tooltip="Plataforma Nacional de Transparencia" width="50" src="img/pnt-l.png">
					</div>
				</a>
			</div>

		</div>

	</div>

	<div class="row">
		
		<div class="col s12 m8">
			<p>La Ley General busca promover, fomentar y difundir la cultura de la transparencia en el ejercicio de la funci&oacute;n p&uacute;blica, el acceso a la informaci&oacute;n, la participaci&oacute;n ciudadana, as&iacute; como la rendici&oacute;n de cuentas, a trav&eacute;s del establecimiento de pol&iacute;ticas p&uacute;blicas y mecanismos que garanticen la publicidad de informaci&oacute;n oportuna, verificable, comprensible, actualizada y completa.</p>
			<p class="right-align section"><small><b>Del derecho de acceso a la informaci&oacute;n<br>Ley de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica del Estado de Zacatecas</b></small></p>
		</div>

	</div>

</div>

<div class="row section">
	<div class="container">
		<div class="col s12">
			<div class='valign-wrapper center-align section'>
				<a class='blue-grey-text'>
					<i class='material-icons'>account_balance</i>
				</a>
				<i class='blue-grey-text'>&nbsp;Fracciones</i>
			</div>
			<div class="divider"></div>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		
		<div class="col s12">

			<div id="articulosFeed"></div>

		</div>

	</div>
	<div class="row grey lighten-3 ten-p border-r-10 border-grey-two">

		<div class="col s12">

			<div id="fraccionesFeed"></div>

		</div>

	</div>
	<div class="row">

		<div class="col s12">

			<div id="archivosFeed"></div>

		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/fracciones/fraccionesFeed.js"></script>