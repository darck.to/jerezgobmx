<div class="row no-m p-r">

	<!--MARCOS-->
	<div class="hide-on-small-only portada-marco-left bgc-blue"></div>
	<div class="hide-on-small-only portada-marco-right bgc-blue"></div>

	<div class="col s12 full-news-vh portada-top-background">
		<div class="col s12 m8 offset-m2 portada-news full-news-n">
			<div class="card transparent z-depth-0 full-h">
				<div class="row full-h">
					<div id="carouselPrincipal" class="carousel carousel-slider z-depth-0 full-h">
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="row no-m">

	<div class="col s12 white">
		
		<div class="card transparent z-depth-0">
			<div class="card-content center-align">
				<span class="card-title blue-text text-darken-4">Departamentos</span>
			</div>
				
			<div class="card-content">
			
				<div class="row">

					<div class="container">

						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Secretaría de Gobierno</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Sindicatura</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">DIF</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Atenci&oacute;n Ciudadana</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Desarrollo Econ&oacute;mico</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Obras P&uacute;blicas</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Tesorer&iacute;a</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col s12 m3">
							<div class="card bgc-blue z-depth-0 border-grey-one handed">
								<div class="card-content no-p">
									<div class="row no-m">
										<div class="col s12 m4 bgc-blue-1 white-text valign-wrapper center-align">
											<h6 class="font-two"><i class="fa fa-building-o height-70px full-w"></i></h6>
										</div>
										<div class="col s12 m8 valign-wrapper center-align">
											<h6 class="white-text full-w">Comunicaci&oacute;n Social</h6>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	</div>

</div>

<div class="row bgc-blue-1">
	
	<div class="col s12 m6 offset-m3">

		<div class="card transparent z-depth-0">
			<div class="card-content center-align">
				<span class="card-title white-text">&Uacute;ltimos Decretos</span>
			</div>
		</div>
			
		<div id="decretosPortada"></div>

	</div>

</div>

<div class="row">

	<a href="https://www.facebook.com/comite.comunitario.3979" target="_blank">

		<div class="col s12 back-participacion">

			<div class="container fiften-p">
				
				<div class="card transparent z-depth-1">
					<div class="card-content center-align">
						<spam class="card-title white blue-text text-darken-4 ten-p border-r-10 border-grey-one">Comites de Participaci&oacute;n Ciudadana</spam>
					</div>
					<div class="card-content white-text">
						
						<div class="row">
							<div class="col s12 m3">
								<img class="right" height="50" src="img/f_logo.png">
							</div>
							<div class="col s12 m9">
								<h5>Conoce m&aacute;s del Fondo para la Infraestructura Social Municipal</h5>
							</div>
						</div>

					</div>
				</div>

			</div>

		</div>

	</a>

</div>

<div class="row no-m">

	<div class="col s12 white lighten-4">
		
		<div class="container">
				
			<div class="card transparent z-depth-0 ">
				<div class="card-content center-align">
					<span class="card-title blue-text text-darken-4">Boletines</span>
				</div>
				<div class="card-content center-align white-text no-m no-p">

					<div id="boletinesPortada" class="row"></div>

				</div>
			</div>

		</div>

	</div>

</div>

<div class="row no-m back-jerez">

	<div class="col s12">
		
		<div class="container">
			
			<div class="card transparent z-depth-0">
				<div class="card-content center-align">
					<span class="card-title blue-text text-darken-4">Cont&aacute;ctanos</span>
				</div>
				<div class="card-content">
					<div class="row">
						<div class="col m4 hide-on-med-and-down">
							<i class="material-icons grey-text font-thi">local_phone</i>
						</div>
						<div class="col s12 m12 l8">
							<form>
								<div class="row no-m white z-depth-1 ten-p border-r-10">
									<div class="col s12 m6 black-text">
										<div class="input-field col s12 m6">
											<input id="nombre" type="text" class="validate" required>
			          						<label for="nombre">Nombre(s)</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="paterno" type="text" class="validate">
			          						<label for="paterno">Apellido Paterno</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="materno" type="text" class="validate">
			          						<label for="materno">Apellido Materno</label>
										</div>
										<div class="input-field col s12 m6">
											<input id="telefono" type="text" class="validate">
			          						<label for="telefono">Tel&eacute;fono</label>
										</div>
									</div>
									<div class="col s12 m6 black-text">
										<div class="input-field col s12">
											<input id="correo" type="text" class="validate">
			          						<label for="correo">Correo @</label>
										</div>
										<div class="input-field col s12">
											<textarea id="mensaje" class="materialize-textarea"></textarea>
											<label for="mensaje">Mensaje</label>
										</div>
									</div>
									<div class="col s12 center-align">						
										<button class="btn z-depth-0 white black-text border-black-one" type="submit" name="action">Enviar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</div>

	</div>

</div>

<div class="row no-m">

	<div class="col s12 white">

		<div class="container fiften-p">
		
			<div class="card transparent z-depth-0">
				<div class="row">
					<div class="col s12 m6 offset-m3">
						<div class="card-content border-grey-one center-align">
							<div class="fb-page" data-href="https://www.facebook.com/gobjerez/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/gobjerez/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gobjerez/">Gobierno Municipal de JEREZ</a></blockquote></div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="row bgc-gray">
	
	<div class="col s12">

		<div class="card transparent z-depth-0">
			<div class="card-content center-align">
				<span class="card-title blue-text text-darken-4">INVITACI&Oacute;NES EN PROCESO COMPETITIVO</span>
			</div>
		</div>
			
		<div id="invitacionesPortada"></div>

	</div>

</div>

<div class="row no-m p-r zindex-9">

	<div class="col s12 m5 l4 mapa-avatar">
		<div class="card <-z-depth-0 bgc-blue-2">
			<div class="row">
				<div class="col s12 m4 center-align">
					<img class="avatar-p" src="img/presidente-a.jpg">
				</div>
				<div class="col m8">
					<blockquote class="white-text">
						<p class="section"><b>ANTONIO ACEVES S&Aacute;NCHEZ</b><p>
						<p>Presidente Municipal</p>
						<p>Jerez</p>
						<p class="section">presidencia@jerez.gob.mx</p>
					</blockquote>
				</div>
			</div>
		</div>
	</div>

</div>


<div class="row no-m">

	<div class="col s12 white no-m no-p">
		
		<div class="card transparent z-depth-0 no-m">
			<div class="card-content no-p">
				<div class="row no-m">
					<div class="col s12 no-m no-p">
						<iframe class="portada-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3682.1143202447342!2d-102.99270858455577!3d22.649525885143248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8682effd71b6cc79%3A0x86c4c01269898e80!2sPresidencia+Municipal+de+Jerez!5e0!3m2!1sen!2smx!4v1542746482014" width="600" height="450" frameborder="0" style="border:0"></iframe>
					</div>
				</div>

			</div>
		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/portadaLoad.js"></script>