<?php

	$sub = $_POST['sub'];

	echo"
		<div class='row no-m'>
			<div class='container'>
				<div class='s12'>
					<div class='card transparent z-depth-0 '>
						<div class='card-content center-align'>
							<span class='card-title blue-text text-darken-4'>Transparencia</span>
							<div class='valign-wrapper blue-grey-text center-align section'><a href='.' class='blue-grey-text'><i class='material-icons'>home</i></a><i>&nbsp;".ucwords($sub)."</i></div>
							<div class='divider'></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	";

	include_once('../templates/transparencia/'.$sub.'.php');

?>