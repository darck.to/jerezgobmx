<div class="container">

	<div class="row">

		<div class="col s12">
		
			<div class="card z-depth-0">

				<div class="card-content">
					
					<span class="card-title">Leyes y reglamentos vigentes</span>

				</div>

			</div>

		</div>
		
		<div class="col s12">

			<table class="striped armonizacion-table">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th class="center-align" colspan="1">DECARGAR/VER</th>
					</tr>
				</thead>

				<tbody id="normativaFeed">
				</tbody>
			</table>

		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/gobierno/normativaFeed.js"></script>