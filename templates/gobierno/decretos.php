<div class="container">
	
	<div class="col s12">
		
		<div class="card z-depth-0">

			<div class="card-content">
				
				<span class="card-title">Decretos</span>

			</div>
			
			<div class="card-content">
				
				<table class="striped armonizacion-table">
					<thead>
						<tr>
							<th>Decreto</th>
							<th class="center-align" colspan="1">DECARGAR/VER</th>
						</tr>
					</thead>

					<tbody id="decretosFeed">
					</tbody>
				</table>

			</div>

		</div>

	</div>

</div>

<script type="text/javascript" src="scripts/decretos/decretosFeed.js"></script>