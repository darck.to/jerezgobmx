<div class="container">

	<div class="col s12">

		<div class="card z-depth-0">

			<div class="card-content">

				<span class="card-title">H. Ayundamiento 2018-2021</span>

			</div>

			<div class="card-content">

				<h6><i>Antonio Aceves S&aacute;nchez</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Presidente Municipal</i></h6>
				<h6><i>Lic. Carolina Salazar Orozco</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;S&iacute;ndica Municipal</i></h6>
				<h6><i>Ing. Luis Ignacio de la Torre Acevedo</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Planeaci&oacute;n y Desarrollo Administrativo</i></h6>
				<h6><i>Lic. Reyna Gabriela Rom&aacute;n Saldaña</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Atenci&oacute;n a Migrantes y Asuntos Internacionales</i></h6>
				<h6><i>Mtro. Roberto Ram&iacute;rez Carrillo</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Educaci&oacute;n, Cultura y Medio Ambiente</i></h6>
				<h6><i>Rutilo Carrera D&iacute;az</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Obras y Servicios P&uacute;blicos, Imagen, Desarrollo Urbano y Patrimonio Hist&oacute;rico</i></h6>
				<h6><i>Mar&iacute;a del Carmen Mar&iacute;n Gamboa</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Desarrollo Econ&oacute;mico y Social, Desarrollo Rural y Agropecuario</i></h6>
				<h6><i>Cristina Muro Caldera</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Grupos Vulnerables, Personas con Discapacidad y Equidad entre G&eacute;neros</i></h6>
				<h6><i>Lic. Jes&uacute;s H&eacute;ctor Cervantes Nieves</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Comercio Formal e Informal</i></h6>
				<h6><i>Rafael Tinajero Berumen</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Organismos Descentralizados y Desconcentrados</i></h6>
				<h6><i>Mar&iacute;a del Roc&iacute;o S&aacute;nchez Moreno</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Turismo, Espect&aacute;culos y Alcoholes</i></h6>
				<h6><i>Diana Marie Sol&iacute;s Rodarte</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Juventud y Deporte</i></h6>
				<h6><i>Raymundo Carrillo Ram&iacute;rez</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Justicia, Derechos Humanos, Rendici&oacute;n de Cuentas y Combate a la Corrupci&oacute;n</i></h6>
				<h6><i>Lic. Gabriela Jacobo Arellano</i><i class="grey-text text-lighten-1">&nbsp;|&nbsp;Comisi&oacute;n de Salud P&uacute;blica y Asistencia Social</i></h6>

			</div>

		</div>

	</div>

</div>
