<div class="container">

	<div class="col s12">

		<div class="card z-depth-0">

			<div class="card-content">

				<span class="card-title">Presidencia Municipal de Jerez <em>494 945 6182</em></span>

			</div>

			<div class="card-content">

				<h6><i">Secretar&iacute;a Particular</i><i class="grey-text text-lighten-1 right">494 945 6182</i></h6>
				<h6><i">Secretar&iacute;a de Gobierno</i><i class="grey-text text-lighten-1 right">494 947 1888</i></h6>
				<h6><i">Tesorer&iacute;a</i><i class="grey-text text-lighten-1 right">494 945 8681</i></h6>
				<h6><i">Recursos Materiales</i><i class="grey-text text-lighten-1 right">494 945 2253</i></h6>
				<h6><i">Registro Civil</i><i class="grey-text text-lighten-1 right">494 945 0514</i></h6>
				<h6><i">Direcci&oacute;n de Obras y Servicios P&uacute;blicas</i><i class="grey-text text-lighten-1 right">494 945 8384</i></h6>
				<h6><i">Subdirecci&oacute;n de Servicios P&uacute;blicos</i><i class="grey-text text-lighten-1 right">494 945 6600</i></h6>
				<h6><i">DIF</i><i class="grey-text text-lighten-1 right">494 945 3253</i></h6>
				<h6><i">Comunicaci&oacute;n Social</i><i class="grey-text text-lighten-1 right">494 945 9533</i></h6>

			</div>

		</div>

	</div>

</div>
