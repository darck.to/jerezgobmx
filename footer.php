<footer class="page-footer bgc-blue-2 white-text">

	<div class="container">
		
		<div class="row">
			
			<div class="col s12 m6 l4">
				<h5>Municipio de Jerez</h5>
				<ul>
					<!--<li><h6><a href="#/tramites" class="white-text navigator">TR&Aacute;MITES</a></h6></li>-->
			        <li><h6><a href="#/gobierno/ayuntamiento" class="white-text navigator" >H. AYUNTAMIENTO</a></h6></li>
			        <li><h6><a href="#/gobierno/directorio" class="white-text navigator" >DIRECTORIO</a></h6></li>
				</ul>
			</div>

			<div class="col s12 m6 l4">
				<h5>Transparencia y Rendici&oacute;n de Cuentas</h5>
				<ul>
			        <li><h6><a href="#/transparencia/obligaciones" class="white-text navigator" >TRANSPARENCIA</a></h6></li>
			        <li><h6><a href="#/transparencia/armonizacion" class="white-text navigator" >ARMONIZACI&Oacute;N CONTABLE</a></h6></li>
			        <li><h6><a href="#/transparencia/licitaciones" class="white-text navigator" >LICITACIONES</a></h6></li>
			    </ul>
			</div>

			<div class="col s12 m6 l4">
				<h5>Plataformas Nacionales</h5>
				<div class="col s12 valign-wrapper">
					<a href="https://www.plataformadetransparencia.org.mx/web/guest/inicio" target="_blank">
						<div class="white icon-circle">
							<img class="circle white" src="img/pnt-l.png"/>
						</div>
					</a>
					<h6 class="right">Plataforma Nacional de Transparencia</h6>
				</div>
				<div class="col s12 valign-wrapper">
					<a href="http://infomexzacatecas.org.mx/Infomex/" target="_blank">
						<div class="white icon-circle">
							<img class="circle white" src="img/infomex-l.png"/>
						</div>
					</a>
					<h6 class="right">Sistema INFOMEX</h6>
				</div>
			</div>

		</div>

	</div>
	<div class="white-text bgc-blue footer-copyright right-align">
		<div class="container">
			<i class="left center-align">Jerez de Garc&iacute;a Salinas, Zacatecas | 494 945 6182 | H. Ayuntamiento 2018-2021</i>
			<i class="right center-align">Avisos: <a href="assets/doc/aviso_privacidad_integral.pdf" target="_blank">Privacidad Integral</a></a></i>
		</div>
	</div>

</footer>