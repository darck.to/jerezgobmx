<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

<!--Meta Description-->
<meta name="description" content="Portal de la Administracion del Municipio de Jerez 2018 - 2021">
<meta name="keywords" content="jerez, zacatecas, ayuntamiento, administracion publica, transparencia, rendicion de cuentas, obras publicas jerez, desarrollo economico jerez, dif jerez, dif">
<meta name="author" content="Antonio Aceves Sanchez">

<link href="estilos/reset.css" type="text/css" rel="stylesheet">
<link href="estilos/estilos.css" type="text/css" rel="stylesheet">

<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
<!--Font Asesome-->
<link type="text/css" rel="stylesheet" href="css/font-awesome.css"  media="screen,projection"/>

<!--Favicon-->
<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="site.webmanifest">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="theme-color" content="#ffffff">

<!--[if lt IE 9]>
    <script src="scripts/html5shiv.js"></script>
<![endif]-->
<title>Municipio de Jerez Administracion 2018 - 2021</title>
</head>