<!-- seccion de headers-->
<?php include_once('headers.php'); ?>

<body class="feria-back">

	<!-- Body -->

	<!--Seccion de Logo y Menu-->
	<div class="row no-m">

		<?php include_once('menu.php'); ?>

	</div>

	<div class="container">
		
		<div class="row no-m">

			<div class='s12'>
				<div class='card transparent z-depth-0 '>
					<div class='card-content center-align'>
						<h1><span class='card-title blue-text text-darken-4'>Feria de Primavera Jerez 2019</span></h1>
						<div class='valign-wrapper blue-grey-text center-align section'><a href='.' class='blue-grey-text'><i class='material-icons'>home</i></a><i>&nbsp;Programa y Multimedia</i></div>
						<div class='divider'></div>
					</div>
				</div>
			</div>

			<div class="col s12 m8 fiften-p">
				<div class='s12'>
					<div class='card transparent z-depth-0 no-m no-p'>
						<div class='card-content center-align no-m no-p grey-text text-darken-2'>
							<h6 class="fw-lighter">&nbsp;Descarga el programa completo aqu&iacute;</h6><a href="assets/doc/programa_feria_2019.pdf" target="_blank"><i class='material-icons'>cloud_download</i></a>
						</div>
					</div>
				</div>
				<div class="section"></div>
				<div class="fiften-p">
					<img class="materialboxed responsive-img" src="img/p_f_19.jpg">
				</div>
			</div>

			<div class="col s12 m4 fiften-p">

				<div class="row">
					
					<h5 class="fw-lighter">Siguenos en las redes sociales</h5>

					<div class="fb-page" data-href="https://www.facebook.com/feriajerez/" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/feriajerez/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/feriajerez/">Feria de Primavera Jerez</a></blockquote></div>

				</div>

				
				<div class="row">
			
					<div class='card transparent z-depth-0 '>
						<div class='card-content center-align'>
							<div class='valign-wrapper blue-grey-text center-align section'><a class='blue-grey-text'>Videoteca</a></div>
							<div class='divider'></div>
						</div>
						<div class="row">
							<div class="col s12">
								<video class="ten-p" src="assets/vid/vid_1.mp4" controls>
									<p>If you are reading this, it is because your browser does not support the HTML5 video element.</p>
								</video>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<video class="ten-p" src="assets/vid/vid_2.mp4" controls>
									<p>If you are reading this, it is because your browser does not support the HTML5 video element.</p>
								</video>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<video class="ten-p" src="assets/vid/vid_3.mp4" controls>
									<p>If you are reading this, it is because your browser does not support the HTML5 video element.</p>
								</video>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<video class="ten-p" src="assets/vid/vid_6.mp4" controls>
									<p>If you are reading this, it is because your browser does not support the HTML5 video element.</p>
								</video>
							</div>
						</div>
						<div class="co s12 center-align">
							<small class="grey-text text-darken-1">Puedes descargar los videos haciendo click derecho sobre ellos</small>
						</div>
					</div>

				</div>

			</div>

		</div>

		<div class="row">

			<div class='card transparent z-depth-0 '>
				<div class='card-content center-align'>
					<div class='valign-wrapper blue-grey-text center-align section'><a class='blue-grey-text'>Galer&iacute;a</div>
					<div class='divider'></div>
				</div>
			</div>
			
			<div class="col s12">
				<div class="carousel carouselFeria">
					<a class="carousel-item" class="handed"><img src="img/feria_1.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_2.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_3.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_4.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_5.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_6.jpg"></a>
					<a class="carousel-item" class="handed"><img src="img/feria_7.jpg"></a>
				</div>
			</div>

		</div>

	</div>


	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>

<!-- seccion de inits-->
<?php include_once('init.php'); ?>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2"></script>